/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package mx.merza.facades.constants;

/**
 * Global class for all MerzaFacades constants.
 */
public class MerzaFacadesConstants extends GeneratedMerzaFacadesConstants
{
	public static final String EXTENSIONNAME = "merzafacades";

	private MerzaFacadesConstants()
	{
		//empty
	}
}
