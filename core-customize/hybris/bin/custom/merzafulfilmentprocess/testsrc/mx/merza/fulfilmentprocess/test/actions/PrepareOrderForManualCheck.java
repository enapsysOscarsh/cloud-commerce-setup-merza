/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package mx.merza.fulfilmentprocess.test.actions;

/**
 * Test counterpart for
 * {@link mx.merza.fulfilmentprocess.actions.order.PrepareOrderForManualCheckAction}
 */
public class PrepareOrderForManualCheck extends TestActionTemp
{
	//EMPTY
}
