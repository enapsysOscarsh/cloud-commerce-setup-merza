/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package mx.merza.initialdata.constants;

/**
 * Global class for all MerzaInitialData constants.
 */
public final class MerzaInitialDataConstants extends GeneratedMerzaInitialDataConstants
{
	public static final String EXTENSIONNAME = "merzainitialdata";

	private MerzaInitialDataConstants()
	{
		//empty
	}
}
