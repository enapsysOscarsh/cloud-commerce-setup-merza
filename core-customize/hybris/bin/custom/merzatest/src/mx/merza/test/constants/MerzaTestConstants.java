/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package mx.merza.test.constants;

/**
 * 
 */
public class MerzaTestConstants extends GeneratedMerzaTestConstants
{

	public static final String EXTENSIONNAME = "merzatest";

}
